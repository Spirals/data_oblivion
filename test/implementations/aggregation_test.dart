import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/aggregation.dart';
import '../utils/dataset.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("aggregationCompress", () {
    group("should compress", () {
      test("with a time window of 2 seconds", () {
        List<Model> models = readLocationDataset("artificial_dataset.txt", useLng: true);
        List<Model> compressed = aggregationCompress(models, 2);
        expect(compressed.length, 11);
        expect(compressed[1].item2, 2.5); // average of (2, 3)
      });

      test("with a time window of 10 seconds", () {
        List<Model> models = readLocationDataset("artificial_dataset.txt");
        List<Model> compressed = aggregationCompress(models, 10);
        expect(compressed.length, 3);
      });

      test("with a time window of 11 seconds", () {
        List<Model> models = readLocationDataset("artificial_dataset.txt");
        List<Model> compressed = aggregationCompress(models, 11);
        expect(compressed.length, 2);
      });
    });

    group("should not compress", () {
      test("with a time window of 0", () {
        List<Model> models = readLocationDataset("artificial_dataset.txt");
        expect(() => aggregationCompress(models, 0), throwsA(predicate((e) => e is ArgumentError && e.message == 'Time window must be strictly superior to 0.')));
      });

      test("with a negative time window", () {
        List<Model> models = readLocationDataset("artificial_dataset.txt");
        expect(() => aggregationCompress(models, -42), throwsA(predicate((e) => e is ArgumentError && e.message == 'Time window must be strictly superior to 0.')));
      });
    });
  });
}