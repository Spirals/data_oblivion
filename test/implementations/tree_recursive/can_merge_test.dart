import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/do_compress.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('canMerge', () {
    group('should not merge models', () {
      test('with model beneath error gradient', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(5, 0.5);
        Model next = const Model(10, 2);

        bool result = canMerge(0.01, m1, m2, next);
        expect(result, false);
      });

      test('with model above error gradient', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(5, 3);
        Model next = const Model(10, 2);

        bool result = canMerge(0.01, m1, m2, next);
        expect(result, false);
      });
    });

    group('should merge models', () {
      test('with model in error gradient', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(5, 1);
        Model next = const Model(10, 2);

        bool result = canMerge(0.01, m1, m2, next);
        expect(result, true);
      });

      test('with model on error gradient upper bound', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(5, 1.5);
        Model next = const Model(10, 2);

        bool result = canMerge(0.5, m1, m2, next);
        expect(result, true);
      });

      test('with model on error gradient lower bound', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(5, 0.5);
        Model next = const Model(10, 2);

        bool result = canMerge(0.5, m1, m2, next);
        expect(result, true);
      });
    });
  });
}
