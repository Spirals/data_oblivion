import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_tree.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('getModelsTreeFrom', () {
    test('should return a leaf with one model', () {
      Model m = const Model(0, 42);
      ModelNode tree = getModelsTreeFrom([m]);
      expect(tree.hasChildren, false);
      expect(tree.model!, m);
    });

    test('should return a simple tree with 2 models', () {
      Model m1 = const Model(0, 42);
      Model m2 = const Model(1, 42.2);
      ModelNode tree = getModelsTreeFrom([m1, m2]);

      expect(tree.hasChildren, true);
      expect(tree.left!.model, m1);
      expect(tree.left!.hasChildren, false);
      expect(tree.right!.model, m2);
      expect(tree.right!.hasChildren, false);
    });

    test('should return a tree with 3 models', () {
      Model m1 = const Model(0, 42);
      Model m2 = const Model(1, 42.2);
      Model m3 = const Model(2, 42.4);
      ModelNode tree = getModelsTreeFrom([m1, m2, m3]);

      expect(tree.left!.left!.model, m1);
      expect(tree.left!.right!.model, m2);
      expect(tree.right!.model, m3);
    });

    test('should return a tree with 4 models', () {
      Model m0 = const Model(0, 1);
      Model m1 = const Model(1, 1);
      Model m2 = const Model(2, 1);
      Model m3 = const Model(3, 1);
      ModelNode tree = getModelsTreeFrom([m0, m1, m2, m3]);

      expect(tree.left!.left!.model, m0);
      expect(tree.left!.right!.model, m1);
      expect(tree.right!.left!.model, m2);
      expect(tree.right!.right!.model, m3);
    });

    test('should return a tree with 7 models', () {
      Model m0 = const Model(0, 0);
      Model m1 = const Model(1, 0);
      Model m2 = const Model(2, 0.42);
      Model m3 = const Model(3, -0.334);
      Model m4 = const Model(4, 0.56);
      Model m5 = const Model(5, 0.152);
      Model m6 = const Model(6, -0.67);
      ModelNode tree = getModelsTreeFrom([m0, m1, m2, m3, m4, m5, m6]);

      // left subtree
      expect(tree.left!.left!.left!.model, m0);
      expect(tree.left!.left!.right!.model, m1);
      expect(tree.left!.right!.left!.model, m2);
      expect(tree.left!.right!.right!.model, m3);

      // right subtree
      expect(tree.right!.left!.left!.model, m4);
      expect(tree.right!.left!.right!.model, m5);
      expect(tree.right!.right!.model, m6);
    });
  });
}