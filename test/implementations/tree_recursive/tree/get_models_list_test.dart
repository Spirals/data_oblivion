import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_list.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("getModelsListFrom", () {
    test('should return one model from a leaf tree', () {
      Model m = const Model(42, 42);
      ModelNode tree = ModelNode(m);
      List<Model> results = getModelsListFrom(tree);
      expect(results, [m]);
    });

    test('should return 2 models from a simple tree', () {
      Model m0 = const Model(0, 0);
      Model m1 = const Model(1, 0);
      ModelNode tree = ModelNode(null, left: ModelNode(m0), right: ModelNode(m1));
      List<Model> results = getModelsListFrom(tree);
      expect(results, [m0, m1]);
    });

    test('should return 3 models', () {
      Model m0 = const Model(0, 0);
      Model m1 = const Model(1, 0);
      Model m2 = const Model(2, 0);
      ModelNode leftTree = ModelNode(null, left: ModelNode(m0), right: ModelNode(m1));
      ModelNode tree = ModelNode(null, left: leftTree, right: ModelNode(m2));

      List<Model> results = getModelsListFrom(tree);
      expect(results, [m0, m1, m2]);
    });
  });
}