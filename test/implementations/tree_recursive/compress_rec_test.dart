import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/epsilon_builder.dart';
import 'package:data_oblivion/implementations/tree_recursive/compress_rec.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_list.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("compressRec", () {
    EpsilonBuilder builder = EpsilonBuilder.linearFromStep(0.1);

    test('should return single model from single leaf tree', () {
      Model m0 = const Model(0, 1);
      ModelNode tree = ModelNode(m0);
      ModelNode compressedTree = compressRec(builder, 0, 1, tree);
      expect(compressedTree.model!, m0);
      expect(compressedTree.hasChildren, false);
    });

    test("should compress 2 first models", () {
      Model m0 = const Model(0, 1);
      Model m1 = const Model(1, 1);
      Model m2 = const Model(2, 1);
      Model m3 = const Model(3, 1);
      ModelNode leftTree = ModelNode(null, left: ModelNode(m0), right: ModelNode(m1));
      ModelNode rightTree = ModelNode(null, left: ModelNode(m2), right: ModelNode(m3));
      ModelNode tree = ModelNode(null, left: leftTree, right: rightTree);

      ModelNode compressedTree = compressRec(builder, 0, 1, tree);
      List<Model> models = getModelsListFrom(compressedTree);
      expect(models.length, 3);
      expect(models[0], m0);
      expect(models[1], m2);
      expect(models[2], m3);
    });

    test("should compress all models", () {
      Model m0 = const Model(0, 1);
      Model m1 = const Model(1, 1);
      Model m2 = const Model(2, 1);
      Model m3 = const Model(3, 1);
      ModelNode leftTree = ModelNode(null, left: ModelNode(m0), right: ModelNode(m1));
      ModelNode rightTree = ModelNode(null, left: ModelNode(m2), right: ModelNode(m3));
      ModelNode tree = ModelNode(null, left: leftTree, right: rightTree);

      ModelNode compressedTree = compressRec(builder, 0, 3, tree);
      List<Model> models = getModelsListFrom(compressedTree);
      expect(models.length, 1);
      expect(models[0], m0);
    });
  });
}