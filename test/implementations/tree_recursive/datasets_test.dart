import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/epsilon_builder.dart';
import 'package:data_oblivion/implementations/tree_recursive/compress_rec.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_list.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_tree.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';
import '../../utils/dataset.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("[Dataset tests]", () {
    test('Should compress privamov user1 latitude data', () {
      List<Model> models = readLocationDataset("privamov-gps-user1");
      ModelNode modelsTree = getModelsTreeFrom(models);
      int size = models.length;
      int compressionTarget = (size / 4).floor();
      EpsilonBuilder builder = EpsilonBuilder.linearFromStep(0.001);

      ModelNode compressedTree = compressRec(builder, 0, compressionTarget, modelsTree);
      List<Model> compressedModels = getModelsListFrom(compressedTree);

      expect(compressedModels.length < models.length, true);
      debugPrint("Original list size: ${models.length}");
      debugPrint("Size gain objective: $compressionTarget");
      debugPrint("---");
      debugPrint("Compressed list size: ${compressedModels.length}");
      debugPrint("Gain: ${(compressedModels.length / models.length * 100).toStringAsFixed(2)}%");
    });
  });
}