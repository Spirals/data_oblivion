import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/do_compress.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('doCompress', () {
    group('should fail', () {
      test('with less than 2 models', () {
        Model m1 = const Model(0, 0);
        expect(() => doCompress(0.5, [m1]), throwsA(predicate((e) => e is ArgumentError && e.message == 'Models list must contain at least 2 models.')));
      });
    });

    group('should not compress', () {
      test('2 models', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(5, 10);
        List<Model> result = doCompress(1, [m1, m2]);
        expect(result, [m1]);
      });

      test('an outlayer', () {
        Model m1 = const Model(0, 5.68);
        Model m2 = const Model(6, 42);
        Model m3 = const Model(8, 5);
        Model m4 = const Model(10, 12);
        List<Model> result = doCompress(1, [m1, m2, m3, m4]);
        expect(result, [m1, m2, m3]);
      });
    });

    group('should compress', () {
      test('3 models with same value', () {
        Model m1 = const Model(0, 13.86);
        Model m2 = const Model(6, 13.86);
        Model m3 = const Model(8, 13.86);
        List<Model> result = doCompress(1, [m1, m2, m3]);
        expect(result, [m1]);
      });

      test('3 models with values included in epsilon', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(5, 5.1);
        Model m3 = const Model(10, 10);
        List<Model> result = doCompress(1, [m1, m2, m3]);
        expect(result, [m1]);
      });

      test('models after an outlayer', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(2, 15);
        Model m3 = const Model(4, 0);
        Model m4 = const Model(6, 0.5);
        Model m5 = const Model(8, 0);

        List<Model> result = doCompress(1, [m1, m2, m3, m4, m5]);
        expect(result, [m1, m2, m3]);
      });

      test('models before an outlayer', () {
        Model m1 = const Model(0, 0);
        Model m2 = const Model(1, 0.42);
        Model m3 = const Model(3, -0.334);
        Model m4 = const Model(6, 5);
        Model m5 = const Model(8, 0);
        Model m6 = const Model(10, 0);

        List<Model> result = doCompress(1, [m1, m2, m3, m4, m5, m6]);
        expect(result, [m1, m3, m4, m5]);
      });

      test('a bunch of models with close to no variation at all', () {
        Model m0 = const Model(0, 0);
        Model m1 = const Model(1, 0);
        Model m2 = const Model(2, 0.42);
        Model m3 = const Model(3, -0.334);
        Model m4 = const Model(4, 0.56);
        Model m5 = const Model(5, 0.152);
        Model m6 = const Model(6, -0.67);
        Model m7 = const Model(7, -0.81);
        Model m8 = const Model(8, -0.35);
        Model m9 = const Model(9, 0);

        List<Model> result = doCompress(1, [m0, m1, m2, m3, m4, m5, m6, m7, m8, m9]);
        expect(result, [m0]);
      });
    });
  });
}
