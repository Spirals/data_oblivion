import 'dart:io';
import 'package:data_oblivion/data_oblivion.dart';

List<Model> readLocationDataset(String filename, {bool useLng = false}) {
  // Use datasets folder from library when called from example app
  String directory = '${Directory.current.path}/${Directory.current.path.contains('example') ? '../' : ''}datasets/';
  List<String> file = File(directory + filename).readAsLinesSync();
  List<Model> models = [];

  for (String line in file) {
    List<String> words = line.split(";");
    Model m = Model(double.parse(words[3]), double.parse(words[useLng ? 2 : 1]));
    models.add(m);
  }

  return models;
}