import 'dart:io';

import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/datasets/apolline_dataset.dart';

/// Outputs a file containing a list of data points that can be used in the
/// example app.
void main() {
  List<ApollineField> fields = [ ApollineField.longitude, ApollineField.latitude, ApollineField.temperatureAM2320C, ApollineField.pressure ];
  DateTime startDate = DateTime.parse("2023-04-03");
  DateTime endDate = DateTime.parse("2023-04-04");

  for (ApollineField field in fields) {
    List<Model> models = readApollineDataset(field, startDate: startDate, endDate: endDate);
    File output = File('apolline_output_${field.name}.dart');
    output.writeAsString(
        "List<List<double>> getRaw${field.name.capitalize()}Models() {"
            "return $models;"
            "}");
  }
}