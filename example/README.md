# Example app

This app allows compressing different datasets using several techniques.

A user interface allows visualizing compressed time series and progress of the associated compression
error; it also allows comparing compression approaches, displaying metrics such as the median error.

![Screenshot of the demo app](docs/screenshot.png)

### Datasets

* Air quality over 24h in Rennes, France

### Compression techniques

* Tree compression
* Random removal
* Quotas
  * Full quota removal
  * Part removal (X samples every Y)