/// Describes the epsilon curve needed for the tree compression algorithm.
enum EpsilonCurveType { exponential, linear, logarithmic }

extension EpsilonCurveUtils on EpsilonCurveType {
  String get label {
    return "${name[0].toUpperCase()}${name.substring(1).toLowerCase()}";
  }

  String get labelWithArticle {
    List<String> vowels = ["a", "e", "i", "o"];
    bool shouldUseAn = vowels.contains(name[0]);
    return "${shouldUseAn ? "an" : "a"} $name";
  }
}
