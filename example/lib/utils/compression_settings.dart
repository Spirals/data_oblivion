import 'dart:convert';

import 'package:example/utils/compression_algorithm.dart';
import 'package:example/utils/epsilon_curve_type.dart';
import 'package:flutter/material.dart';


/// Represents a set of settings that is being used in a compression experiment.
///
/// Since this is serialized at runtime to avoid passing references by mistake,
/// don't forget to include your new variables to `toJson` and `fromJson`
/// methods.
///
/// If changing a value of the current instance must trigger a UI rebuild (if
/// the value is used as model for a dropdown list, for instance), it must be
/// encapsulated in a `ValueNotifier` object, and call the `notifyListeners`
/// method on change; check `epsilonCurveType` implementation as reference.
class CompressionSettings with ChangeNotifier {
  CompressionAlgorithm? algorithm;
  late final ValueNotifier<EpsilonCurveType?> epsilonCurveType;
  double step = -1;
  int sizeGain = -1;
  int removedModelsCount = -1;
  int sampleSize = -1;
  double quota = -1;
  int windowTime = -1;
  double maxError = -1;

  CompressionSettings(
      this.algorithm,
      this.step,
      this.sizeGain,
      {this.removedModelsCount = -1, this.sampleSize = -1, this.quota = -1, this.windowTime = -1, this.maxError = -1, EpsilonCurveType? epsilonCurveType}
      ) {
    this.epsilonCurveType = ValueNotifier(epsilonCurveType);
    this.epsilonCurveType.addListener(() {
      notifyListeners();
    });
  }

  factory CompressionSettings.empty() {
    return CompressionSettings(null, -1, -1);
  }

  /// Creates a copy (copying the values, and not the references) of a
  /// `CompressionSettings` instance.
  factory CompressionSettings.from(CompressionSettings other) {
    String serialized = jsonEncode(other.toJson());
    return CompressionSettings.fromJson(jsonDecode(serialized));
  }

  /// Converts the current instance to a JSON map object.
  Map<String, dynamic> toJson() {
    return {
      'algo': algorithm == null ? -1 : algorithm!.index,
      'epsilonType': epsilonCurveType.value == null ? -1 : epsilonCurveType.value!.index,
      'step': step,
      'sizeGain': sizeGain,
      'removedModelsCount': removedModelsCount,
      'sampleSize': sampleSize,
      'quota': quota,
      'windowTime': windowTime,
      'maxError': maxError
    };
  }

  /// Rebuilds a `CompressionSettings` instance from a JSON map object.
  factory CompressionSettings.fromJson(Map<String, dynamic> json) {
    return CompressionSettings(
        json['algo'] == -1 ? null : CompressionAlgorithm.values[json['algo']],
        json['step'],
        json['sizeGain'],
      epsilonCurveType: json['epsilonType'] == -1
          ? null
          : EpsilonCurveType.values[json['epsilonType']],
      removedModelsCount: json['removedModelsCount'],
      sampleSize: json['sampleSize'],
      quota: json['quota'],
      windowTime: json['windowTime'],
      maxError: json['maxError']
    );
  }

  /// Checks if current settings can be used to compress data.
  ///
  /// This method will only throw if the compression algorithm is not selected,
  /// the most important of the settings verification is done in the
  /// `CompressionAlgorithm` package (all compression algorithm related code is
  /// located in this package to ease addition of new compression algorithms).
  void verify() {
    CompressionAlgorithm? algo = algorithm;
    if (algo == null) {
      throw ArgumentError("No compression algorithm is selected.");
    }
    algo.verify(this);
  }

  /// Returns input components associated to current settings.
  ///
  /// This method will return an empty list if the compression algorithm is not
  /// selected, then summon the `CompressionAlgorithm` package to get input
  /// components (all compression algorithm related code is located in this
  /// package to ease addition of new compression algorithms).
  List<Widget> getInputWidgets() {
    CompressionAlgorithm? algo = algorithm;
    if (algo == null) {
      return [];
    }
    return algo.getInputWidgets(this);
  }
}
