import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/time_series.dart';
import 'package:data_oblivion/timeseries_distance.dart';
import 'package:example/utils/compression_algorithm.dart';
import 'package:example/utils/compression_settings.dart';
import 'package:filesize/filesize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_randomcolor/flutter_randomcolor.dart';

class Experiment {
  final String name;
  final CompressionSettings settings;
  late final TimeSeries timeSeries;
  late final TimeSeriesDistance? distance;
  final int duration; // in milliseconds
  late final Color color;
  bool display = true;
  final bool isOriginalDataset;

  // Ensure no experiments have the same color
  static final List<Color> _usedColors = [Colors.red];

  Experiment(List<Model> originalDataset, List<Model> compressed, {required this.name, this.isOriginalDataset = false, required this.settings, required this.duration}) {
    if (isOriginalDataset) {
      color = Colors.red;
      timeSeries = TimeSeries.pla(originalDataset);
    } else {
      // Color computing
      Color tempColor = Colors.red;
      while (_usedColors.contains(tempColor)) {
        String hexColor = RandomColor.getColor(Options(format: Format.hex, luminosity: Luminosity.dark));
        hexColor = hexColor.replaceAll("#", "0xFF");
        tempColor = Color(int.parse(hexColor));
      }
      color = tempColor;
      _usedColors.add(tempColor);

      // Compute distance
      timeSeries = settings.algorithm!.associatedTimeseries(compressed);
      distance = TimeSeriesDistance(TimeSeries.pla(originalDataset), timeSeries, originalDataset.map((e) => e.item1).toList());
    }
  }

  factory Experiment.original(List<Model> data) {
    return Experiment(
        data, [],
        name: "Original dataset",
        settings: CompressionSettings.empty(),
        duration: -1,
        isOriginalDataset: true);
  }

  int get pointsCount {
    return timeSeries.models.length;
  }

  String get humanReadableSize {
    return filesize(timeSeries.models.length * 2 * 64);
  }

  String get results {
    if (distance == null) {
      return "";
    }

    if (!distance!.isDone) {
      return "Computing metrics...";
    }

    return "Value error: median error=${distance!.median.toStringAsFixed(2)}, "
        "90e percentile=${distance!.getPercentile(0.9).toStringAsFixed(2)}, "
        "MSE=${distance!.mse.toStringAsFixed(2)}, "
        "RMSE=${distance!.rmse.toStringAsFixed(2)}";
  }

  String get settingsDescription {
    return settings.algorithm!.description(settings, duration);
  }
}