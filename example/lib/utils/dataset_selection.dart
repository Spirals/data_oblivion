import 'package:example/state/model.dart';
import 'package:flutter/material.dart';
import 'package:data_oblivion/modelling/dataset.dart';
import 'package:data_oblivion/modelling/datasets/ecg_dataset.dart';
import 'package:data_oblivion/modelling/datasets/space_shuttle_dataset.dart';
import 'package:data_oblivion/modelling/datasets/artificial_dataset.dart';
import 'package:data_oblivion/modelling/datasets/cabspotting_dataset.dart';
import 'package:data_oblivion/modelling/datasets/privamov_dataset.dart';
import 'package:data_oblivion/modelling/datasets/apolline_dataset.dart';
import 'package:provider/provider.dart';

Future<void> openDatasetSelectionDialog(BuildContext context) {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return Consumer<AppModel>(
        builder: (context, settings, child) {
          // Dataset selection
          if (!settings.loading && settings.data.isEmpty) {
            return _getSelectionDialog(context);
          }

          // Loading dataset
          if (settings.data.isEmpty) {
            return _getDatasetLoadingDialog();
          }

          // Else hide dialog
          Future.delayed(Duration.zero, () => Navigator.of(context).pop());
          return const Text("ok");
        });
    },
  );
}

List<DropdownMenuItem<Dataset>>? _getDropdownItems() {
  // Add one time series per Apolline field
  List<DropdownMenuItem<Dataset>> apollineItems = ApollineField.values
    .where((element) => element != ApollineField.time)
    .map((e) => DropdownMenuItem<Dataset>(
      value: ApollineDataset(field: e),
      child: Text('Apolline [${e.label}]'),
    )
  ).toList();
  apollineItems.sort((a, b) => (a.child as Text).data!.compareTo((b.child as Text).data!));

  // Add Cabspotting user0 latitude and longitude time series
  List<DropdownMenuItem<Dataset>> cabspottingItems = [
    DropdownMenuItem(
      value: CabspottingDataset(useLatitude: true),
      child: const Text("Cabspotting's user 0 (latitude)"),
    ),
    DropdownMenuItem(
      value: CabspottingDataset(useLatitude: false),
      child: const Text("Cabspotting's user 0 (longitude)"),
    )
  ];

  // ECG dataset
  DropdownMenuItem<Dataset> ecgItem = DropdownMenuItem(
    value: ECGDataset(),
    child: const Text("ECG dataset"),
  );

  // Space shuttle dataset
  DropdownMenuItem<Dataset> spaceShuttleItem = DropdownMenuItem(
    value: SpaceShuttleDataset(),
    child: const Text("Space shuttle Marotta valve dataset"),
  );

  // Add Privamov user1 latitude and longitude time series
  List<DropdownMenuItem<Dataset>> privamovItems = [
    DropdownMenuItem(
      value: PrivamovDataset(useLatitude: true),
      child: const Text("Privamov's user 1 (latitude)"),
    ),
    DropdownMenuItem(
      value: PrivamovDataset(useLatitude: false),
      child: const Text("Privamov's user 1 (longitude)"),
    )
  ];

  // Artificial latitude and longitude time series
  List<DropdownMenuItem<Dataset>> artificialItems = [
    DropdownMenuItem(
      value: ArtificialDataset(useLatitude: true),
      child: const Text("Test series (latitude)"),
    ),
    DropdownMenuItem(
      value: ArtificialDataset(useLatitude: false),
      child: const Text("Test series (longitude)"),
    )
  ];

  return [...apollineItems, ...cabspottingItems, ecgItem, spaceShuttleItem, ...privamovItems, ...artificialItems];
}

Widget _getSelectionDialog(BuildContext context) {
  return AlertDialog(
    title: const Text('Select a dataset'),
    content: const SingleChildScrollView(
      child: ListBody(
        children: <Widget>[
          Text('Please select a dataset.'),
          Text('This dataset will be used to compare compression algorithms.'),
        ],
      ),
    ),
    actions: [
      DropdownButton<Dataset>(
        underline: Container(),
        isExpanded: true,
        padding:
        const EdgeInsets.only(top: 5, right: 5, bottom: 5, left: 10),
        hint: const Text("Select dataset"),
        items: _getDropdownItems(),
        onChanged: (value) => _onDatasetSelected(value!, context),
      ),
    ],
  );
}

void _onDatasetSelected(Dataset dataset, BuildContext context) async {
  Provider.of<AppModel>(context, listen: false).dataset = dataset;
}

AlertDialog _getDatasetLoadingDialog() {
  return const AlertDialog(
    title: Text("Select a dataset"),
    content: ListTile(
      title: Text("Loading dataset..."),
      trailing: CircularProgressIndicator(),
    ),
  );
}