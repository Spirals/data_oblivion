import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/epsilon_builder.dart';
import 'package:data_oblivion/implementations/acpa.dart';
import 'package:data_oblivion/implementations/aggregation.dart';
import 'package:data_oblivion/implementations/removal.dart';
import 'package:data_oblivion/modelling/time_series.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_list.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_tree.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';
import 'package:data_oblivion/implementations/tree_recursive/compress_rec.dart';
import 'package:example/utils/compression_settings.dart';
import 'package:example/utils/epsilon_curve_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:temporaldb/models/fast_linear_interpolation.dart';

/// This describes the compression algorithms that are usable in the demo
/// application.
enum CompressionAlgorithm {
  acpa,
  aggregation,
  fli,
  quota,
  tree,
  regularRemoval,
  randomRemoval
}

extension CompressionAlgorithmUtils on CompressionAlgorithm {
  String get name {
    switch (this) {
      case CompressionAlgorithm.acpa:
        return "Adaptive piecewise constant approximation";
      case CompressionAlgorithm.tree:
        return "Tree compression";
      case CompressionAlgorithm.regularRemoval:
        return "Regular removal";
      case CompressionAlgorithm.randomRemoval:
        return "Random deletion";
      case CompressionAlgorithm.quota:
        return "Quota removal";
      case CompressionAlgorithm.aggregation:
        return "Time aggregation";
      case CompressionAlgorithm.fli:
        return "Fast linear interpolation";
      default:
        throw UnimplementedError("Unknown compression algorithm");
    }
  }

  /// Compresses a list of model regarding the current compression algorithm.
  List<Model> compress(List<Model> models, CompressionSettings settings) {
    switch (this) {
      case CompressionAlgorithm.acpa:
        return acpa(models, settings.maxError);
      case CompressionAlgorithm.tree:
        ModelNode tree = getModelsTreeFrom(models);
        EpsilonBuilder? builder;

        switch (settings.epsilonCurveType.value) {
          case EpsilonCurveType.exponential:
            builder = EpsilonBuilder.exponential;
            break;
          case EpsilonCurveType.logarithmic:
            builder = EpsilonBuilder.logarithmic;
            break;
          case EpsilonCurveType.linear:
            builder = EpsilonBuilder.linearFromStep(settings.step);
            break;
          default:
            throw UnimplementedError("Unknown epsilon curve type.");
        }

        ModelNode compressed = compressRec(builder, 0, settings.sizeGain, tree);
        return getModelsListFrom(compressed);
      case CompressionAlgorithm.regularRemoval:
        return regularRemovalCompress(models, settings.removedModelsCount, settings.sampleSize);
      case CompressionAlgorithm.randomRemoval:
        return randomRemovalCompress(models);
      case CompressionAlgorithm.quota:
        return quotaRemovalCompress(models, settings.quota);
      case CompressionAlgorithm.aggregation:
        return aggregationCompress(models, settings.windowTime);
      case CompressionAlgorithm.fli:
        FastLinearInterpolation fliModel = FastLinearInterpolation();
        fliModel.setParameters({'error': settings.maxError.toString()});
        for (Model m in models) {
          fliModel.add(m.item1, m.item2);
        }
        List<Model> result = fliModel.data.map((e) => Model(e.item1, e.item2)).toList();
        return result;
      default:
        throw UnimplementedError("Unknown compression algorithm.");
    }
  }

  /// Describes the compression settings configuration. This is used to describe
  /// experiments by displaying associated settings in tooltips.
  String description(CompressionSettings settings, int duration) {
    String desc = "";

    switch (settings.algorithm!) {
      case CompressionAlgorithm.fli:
      case CompressionAlgorithm.acpa:
        desc = "This used a tolerated error of ${settings.maxError}";
        break;
      case CompressionAlgorithm.tree:
        desc = settings.epsilonCurveType.value == EpsilonCurveType.linear
            ? "This used ${settings.epsilonCurveType.value!.labelWithArticle} builder with a step of ${settings.step}"
            : "This used ${settings.epsilonCurveType.value!.labelWithArticle} builder";
      case CompressionAlgorithm.regularRemoval:
        desc = "This removed ${settings.removedModelsCount} points every ${settings.sampleSize} data samples";
      case CompressionAlgorithm.randomRemoval:
        desc = "This used a random deletion algorithm (one chance out of two to remove each value)";
      case CompressionAlgorithm.quota:
        desc = "Freed ${(settings.quota*100).toStringAsFixed(2)}% of all models";
      case CompressionAlgorithm.aggregation:
        desc = "Aggregation size is ${settings.windowTime}";
      default:
        throw UnimplementedError("Unknown experiment type.");
    }

    desc += "; compression was done in $duration ms.";

    return desc;
  }

  /// Ensures all required settings for a given compression algorithm are
  /// correctly set, throws otherwise.
  void verify(CompressionSettings settings) {
    switch (settings.algorithm!) {
      case CompressionAlgorithm.fli:
        if (settings.maxError == -1) {
          throw ArgumentError("Please input a maximum error.");
        }
        break;
      case CompressionAlgorithm.acpa:
        if (settings.maxError == -1) {
          throw ArgumentError("Please input a maximum error.");
        }
        if (settings.maxError <= 0) {
          throw ArgumentError("Maximum error must be strictly superior to zero.");
        }
        break;
      case CompressionAlgorithm.tree:
        // Epsilon curve selection
        if (settings.epsilonCurveType.value == null) {
          throw ArgumentError("Please select an epsilon function type.");
        }

        // No linear epsilon parameter set
        if (settings.epsilonCurveType.value == EpsilonCurveType.linear && settings.step == -1) {
          throw ArgumentError("Please enter an epsilon function step.");
        }

        // No size target
        if (settings.sizeGain == -1) {
          throw ArgumentError("Please enter a size target.");
        }
        break;
      case CompressionAlgorithm.regularRemoval:
        // No to-be-removed models count
        if (settings.removedModelsCount == -1) {
          throw ArgumentError("Please enter a count of model to remove.");
        }

        // No sample size
        if (settings.sampleSize == -1) {
          throw ArgumentError("Please enter a sample size.");
        }

        // Sample size inferior to removal sample size
        if (settings.removedModelsCount >= settings.sampleSize) {
          throw ArgumentError("The count of models to be removed must be strictly inferior to the sample size.");
        }
        break;
      case CompressionAlgorithm.randomRemoval:
        // This algorithm does not need any input
        return;
      case CompressionAlgorithm.quota:
        if (settings.quota <= 0 || settings.quota >= 1) {
          throw ArgumentError("Quota must be strictly included between 0 and 100.");
        }
        break;
      case CompressionAlgorithm.aggregation:
        if (settings.windowTime <= 0) {
          throw ArgumentError('Time window must be strictly superior to 0.');
        }
        break;
    }
  }

  List<Widget> getInputWidgets(CompressionSettings settings) {
    CompressionAlgorithm algo = settings.algorithm!;
    List<Widget> widgets = [];

    switch(algo) {
      case CompressionAlgorithm.tree:
        Container epsilonBuilderTypeSelector = Container(
            height: 43,
            color: Colors.grey.shade50,
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: Colors.grey.shade50, //background color of dropdown button
                border: Border.all(
                    color: Colors.black38, width: 1), //border of dropdown button
                borderRadius: BorderRadius.circular(5),
              ),
              child: DropdownButton<EpsilonCurveType>(
                underline: Container(),
                isExpanded: true,
                padding:
                const EdgeInsets.only(top: 5, right: 5, bottom: 5, left: 10),
                hint: const Text("Select epsilon function type"),
                value: settings.epsilonCurveType.value,
                items: EpsilonCurveType.values
                    .map((e) => DropdownMenuItem<EpsilonCurveType>(
                    value: e, child: Text(e.label)))
                    .toList(),
                onChanged: (EpsilonCurveType? value) {
                  settings.epsilonCurveType.value = value;
                },
              ),
            ));

        TextField algoSetting = TextField(
          maxLines: 1,
          decoration: const InputDecoration(
              isDense: true, border: OutlineInputBorder(), hintText: "Step"),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            TextInputFormatter.withFunction((oldValue, newValue) {
              if (newValue.text.isEmpty) {
                return newValue;
              }
              try {
                double.parse(newValue.text);
                return newValue;
              } catch (err) {
                return oldValue;
              }
            })
          ],
          onChanged: (String value) {
            settings.step = value.isEmpty ? -1 : double.parse(value);
          },
        );

        widgets.add(Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Row(
            children: settings.epsilonCurveType.value == EpsilonCurveType.linear
                ? [
              Expanded(flex: 2, child: epsilonBuilderTypeSelector),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: algoSetting,
                  ))
            ]
                : [Expanded(flex: 2, child: epsilonBuilderTypeSelector)],
          ),
        ));

        // Number of models
        TextField modelsNumber = TextField(
          decoration: const InputDecoration(
              isDense: true,
              border: OutlineInputBorder(),
              hintText: "Enter a number of models you want to gain"),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          onChanged: (String value) {
            settings.sizeGain = value.isEmpty ? -1 : int.parse(value);
          },
        );
        widgets.add(modelsNumber);
        break;
      case CompressionAlgorithm.regularRemoval:
        TextField removedModelsCountSetting = TextField(
          maxLines: 1,
          decoration: const InputDecoration(
              isDense: true, border: OutlineInputBorder(), hintText: "Models count"),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            TextInputFormatter.withFunction((oldValue, newValue) {
              if (newValue.text.isEmpty) {
                return newValue;
              }
              try {
                int.parse(newValue.text);
                return newValue;
              } catch (err) {
                return oldValue;
              }
            })
          ],
          onChanged: (String value) {
            settings.removedModelsCount = value.isEmpty ? -1 : int.parse(value);
          },
        );

        TextField sampleSizeSetting = TextField(
          maxLines: 1,
          decoration: const InputDecoration(
              isDense: true, border: OutlineInputBorder(), hintText: "Sample size"),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            TextInputFormatter.withFunction((oldValue, newValue) {
              if (newValue.text.isEmpty) {
                return newValue;
              }
              try {
                int.parse(newValue.text);
                return newValue;
              } catch (err) {
                return oldValue;
              }
            })
          ],
          onChanged: (String value) {
            settings.sampleSize = value.isEmpty ? -1 : int.parse(value);
          },
        );

        widgets.add(Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Row(
            children: [
              Expanded(flex: 2, child: removedModelsCountSetting),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 5),
                child: const Text("removed out of"),
              ),
              Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: sampleSizeSetting,
                  ))
            ],
          ),
        ));
        break;
      case CompressionAlgorithm.randomRemoval:
        // This algorithm does not need any input
        break;
      case CompressionAlgorithm.quota:
        TextField quotaInput = TextField(
          maxLines: 1,
          decoration: const InputDecoration(
              isDense: true, border: OutlineInputBorder(), hintText: "Quota (%) to remove"),
          // keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            TextInputFormatter.withFunction((oldValue, newValue) {
              if (newValue.text.isEmpty) {
                return newValue;
              }
              try {
                double.parse(newValue.text);
                return newValue;
              } catch (err) {
                return oldValue;
              }
            })
          ],
          onChanged: (String value) {
            settings.quota = value.isEmpty ? -1 : double.parse(value)/100;
          },
        );
        widgets.add(quotaInput);
        break;
      case CompressionAlgorithm.aggregation:
        TextField quotaInput = TextField(
          maxLines: 1,
          decoration: const InputDecoration(
              isDense: true, border: OutlineInputBorder(), hintText: "Time window size"),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            TextInputFormatter.withFunction((oldValue, newValue) {
              if (newValue.text.isEmpty) {
                return newValue;
              }
              try {
                int.parse(newValue.text);
                return newValue;
              } catch (err) {
                return oldValue;
              }
            })
          ],
          onChanged: (String value) {
            settings.windowTime = value.isEmpty ? -1 : int.parse(value);
          },
        );
        widgets.add(quotaInput);
        break;

      case CompressionAlgorithm.acpa:
      case CompressionAlgorithm.fli:
        TextField maxErrorInput = TextField(
          maxLines: 1,
          decoration: const InputDecoration(
              isDense: true, border: OutlineInputBorder(), hintText: "Maximum error"),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            TextInputFormatter.withFunction((oldValue, newValue) {
              if (newValue.text.isEmpty) {
                return newValue;
              }
              try {
                double.parse(newValue.text);
                return newValue;
              } catch (err) {
                return oldValue;
              }
            })
          ],
          onChanged: (String value) {
            settings.maxError = value.isEmpty ? -1 : double.parse(value);
          },
        );
        widgets.add(maxErrorInput);
        break;
      default:
        throw UnimplementedError("Cannot build input widgets for unknown compression algorithm.");
    }

    return widgets;
  }

  TimeSeries associatedTimeseries(List<Model> models) {
    switch (this) {
      case CompressionAlgorithm.quota:
      case CompressionAlgorithm.tree:
      case CompressionAlgorithm.regularRemoval:
      case CompressionAlgorithm.randomRemoval:
      case CompressionAlgorithm.fli:
        return TimeSeries.pla(models);
      case CompressionAlgorithm.aggregation:
      case CompressionAlgorithm.acpa:
        return TimeSeries.paa(models);
    }
  }
}
