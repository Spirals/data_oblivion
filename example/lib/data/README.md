# Dataset files

This directory contains Apolline dataset files that are used by the example app.

The script located at `data_oblivion/test/resource/apolline.dart` can be used to generate such 
files.