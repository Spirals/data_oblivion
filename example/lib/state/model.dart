import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';
import 'package:flutter/cupertino.dart';

class AppModel extends ChangeNotifier {
  bool _loading = false;
  Dataset? _dataset;
  List<Model> _data = [];
  bool _displayErrorSlope = false;
  bool _showCharts = true;

  set dataset (Dataset? value) {
    _dataset = value;
    _loading = true;
    notifyListeners();
    _dataset!.data.then((value) {
      _loading = false;
      _data = value;
      notifyListeners();
    });
  }
  Dataset? get dataset {
    return _dataset;
  }

  bool get loading {
    return _loading;
  }
  List<Model> get data {
    return _data;
  }

  bool get errorSlope {
    return _displayErrorSlope;
  }
  set errorSlope(bool value) {
    _displayErrorSlope = value;
    notifyListeners();
  }

  bool get displayCharts {
    return _showCharts;
  }
  set displayCharts(bool value) {
    _showCharts = value;
    notifyListeners();
  }

  void reset() {
    _dataset = null;
    _loading = false;
    _data = [];
    _displayErrorSlope = false;
    notifyListeners();
  }
}