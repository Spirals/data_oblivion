import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';
import 'package:data_oblivion/timeseries_distance.dart';
import 'package:example/state/model.dart';
import 'package:example/utils/compression_algorithm.dart';
import 'package:example/utils/compression_settings.dart';
import 'package:example/utils/dataset_selection.dart';
import 'package:example/utils/experiment.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class _CompressionPayload {
  int time;
  Map<String, dynamic> settings;
  List<Model> dataset;
  _CompressionPayload(this.time, this.settings, this.dataset);
}

class ChartView extends StatefulWidget {
  const ChartView({super.key});

  @override
  State<StatefulWidget> createState() => _ChartViewState();
}

class _ChartViewState extends State<ChartView> {
  List<Experiment> experiments = [];

  bool isCompressing = false;
  TimeSeriesDistance? distance;
  CompressionSettings settings = CompressionSettings.empty();

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AppModel>(
        builder: (context, aSettings, child) {
          List<Widget> widgets = [];

          // Open dataset selection dialog if needed
          if (aSettings.data.isEmpty && !aSettings.loading) {
            experiments = [];
            settings = CompressionSettings.empty();
            Future.delayed(Duration.zero, () => openDatasetSelectionDialog(context));
          }

          // Register original dataset
          if (experiments.isEmpty && aSettings.data.isNotEmpty) {
            experiments.add(Experiment.original(aSettings.data));
            debugPrint("Loaded ${aSettings.data.length} points.");
          }

          // Experiments list
          Widget controls = Expanded(
            flex: 4,
            child: Row(
              children: [
                Expanded(
                  flex: 4,
                  child: Container(
                    margin: aSettings.displayCharts ? const EdgeInsets.only(left: 20, top: 20) : const EdgeInsets.all(20),
                    child: _buildButtons(),
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: _buildExperimentsList(),
                ),
              ],
            ),
          );
          widgets.add(controls);

          // Add charts widget if needed
          if (aSettings.displayCharts) {
            Widget charts = Expanded(
              flex: 9,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 25),
                child: _buildChart(),
              ),
            );
            widgets.insert(0, charts);
          }

          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: widgets,
            ),
          );
        });
  }

  /// Returns X axis for the currently displayed dataset.
  ///
  /// If dataset is time indexed, this will display time labels every 2 hours;
  /// otherwise, it will display 10 data indexes.
  AxisTitles _getXAxisTitles() {
    AppModel state = Provider.of<AppModel>(context, listen: false);
    Dataset? dataset = state.dataset;
    if (dataset == null) {
      return const AxisTitles();
    }

    return AxisTitles(
        sideTitles: SideTitles(
            showTitles: true,
            interval: dataset.isTimeIndexed
                ? 2 * 60 * 60 * 1000 // 2 hours
                : state.data.isEmpty ? 1000 : state.data.length / 10,
            getTitlesWidget: (double value, TitleMeta meta) {
              String content = value.toString();

              if (dataset.isTimeIndexed) {
                DateTime time = DateTime.fromMillisecondsSinceEpoch(value.round());
                String formattedDate = DateFormat('kk:mm').format(time);
                if (formattedDate == "24:00") {
                  formattedDate = "0:00";
                }
                content = formattedDate;
              }

              return Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Text(content, textAlign: TextAlign.center));
            },
            reservedSize: 30));
  }

  Widget _buildChart() {
    List<Model> dataset = Provider.of<AppModel>(context, listen: false).data;
    List<LineChartBarData> dataSeries = experiments
        .where((element) => element.display)
        .map((e) => LineChartBarData(
            barWidth: e.isOriginalDataset ? 2 : 4,
            dotData: const FlDotData(show: false),
            spots: e.timeSeries.chartData(dataset.first.item1, dataset.last.item1).map((e) => FlSpot(e.item1, e.item2)).toList(),
            color: e.color))
        .toList();

    return Consumer<AppModel>(
      builder: (context, settings, child) {
        LineChart dataChart = LineChart(
          LineChartData(
            lineTouchData: const LineTouchData(enabled: false),
            titlesData: FlTitlesData(
                show: true,
                topTitles: const AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                rightTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      reservedSize: settings.errorSlope && experiments.length > 1 ? 35 : 0,
                      getTitlesWidget: (_, __) => const Text(""),
                    )
                ),
                bottomTitles: _getXAxisTitles()),
            lineBarsData: dataSeries,
          ),
        );

        if (!settings.errorSlope) {
          return dataChart;
        } else {
          List<LineChartBarData> errorSeries = experiments
              .where((element) => !element.isOriginalDataset && element.display && element.distance != null)
              .map((e) => LineChartBarData(
            barWidth: 2,
            dotData: const FlDotData(show: false),
            dashArray: [10, 6],
            spots: e.distance!.errors.asMap().entries.map((entry) => FlSpot(settings.data[entry.key].item1, entry.value)).toList(),
            color: e.color.withAlpha(80),
          ))
              .toList();

          LineChart errorChart = LineChart(
            LineChartData(
              lineTouchData: const LineTouchData(enabled: false),
              gridData: const FlGridData(
                  show: false
              ),
              titlesData: FlTitlesData(
                  show: true,
                  topTitles: const AxisTitles(
                    sideTitles: SideTitles(showTitles: false),
                  ),
                  leftTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      reservedSize: 44,
                      getTitlesWidget: (_, __) => const Text(""),
                    ),
                  ),
                  rightTitles: AxisTitles(
                      sideTitles: SideTitles(
                          showTitles: true,
                          reservedSize: settings.errorSlope && experiments.length > 1 ? 35 : 0,
                          getTitlesWidget: (double value, _) {
                            return Container(
                              margin: const EdgeInsets.only(top: 10, left: 5),
                              child: Text(value.toStringAsFixed(2)),
                            );
                          }
                      )
                  ),
                  bottomTitles: AxisTitles(
                      sideTitles: SideTitles(
                        showTitles: true,
                        reservedSize: 30,
                        getTitlesWidget: (_, __) => const Text(""),
                      )
                  )
              ),
              lineBarsData: errorSeries,
            ),
          );

          return Stack(
            children: [
              dataChart,
              errorChart
            ],
          );
        }
      },
    );
  }

  Widget _buildExperimentsList() {
    return ListView(
      children: experiments.map((e) {
        String desc = "${e.pointsCount} points (${(e.humanReadableSize)})";
        String acronym = e.name
            .split(" ")
            .map((e) => e.substring(0, 1))
            .join()
            .toUpperCase();

        Widget leading = CircleAvatar(
            backgroundColor: e.display ? e.color : Colors.grey.shade500,
            child: Text(acronym,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: acronym.length > 3 ? 12 : 14),
            ));
        if (!e.isOriginalDataset) {
          leading = Tooltip(
            message: e.settingsDescription,
            child: leading,
          );
        }

        Widget? trailing = e.isOriginalDataset
            ? null
            : IconButton(
          icon: e.display
              ? const Icon(Icons.gps_off)
              : const Icon(Icons.gps_fixed_outlined),
          onPressed: () {
            setState(() {
              e.display = !e.display;
            });
          },
        );

        if (e.isOriginalDataset) {
          return ListTile(
            leading: leading,
            trailing: trailing,
            title: Text(e.name),
            subtitle: Text(desc),
            enabled: e.display,
          );
        } else {
          return ListenableBuilder(
              listenable: e.distance!,
              builder: (_, __) {
                String desc =
                    "${e.pointsCount} points (${(e.humanReadableSize)})";
                if (e.results.isNotEmpty) {
                  desc += "\n${e.results}";
                }

                return ListTile(
                  leading: leading,
                  trailing: e.distance!.isDone
                      ? trailing
                      : const CircularProgressIndicator(),
                  title: Text(e.name),
                  subtitle: Text(desc),
                  enabled: e.display,
                );
              });
        }
      }).toList(),
    );
  }

  Widget _buildButtons() {
    List<Widget> widgets = [];

    // Algorithm selection
    Container algorithmSelector = Container(
        height: 43,
        padding: const EdgeInsets.only(bottom: 5),
        color: Colors.grey.shade50,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.grey.shade50, //background color of dropdown button
            border: Border.all(
                color: Colors.black38, width: 1), //border of dropdown button
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButton<CompressionAlgorithm>(
            underline: Container(),
            isExpanded: true,
            padding:
                const EdgeInsets.only(top: 5, right: 5, bottom: 5, left: 10),
            hint: const Text("Select compression algorithm"),
            value: settings.algorithm,
            items: CompressionAlgorithm.values
                .map((e) => DropdownMenuItem<CompressionAlgorithm>(
                    value: e, child: Text(e.name)))
                .toList(),
            onChanged: (CompressionAlgorithm? value) {
              setState(() {
                settings = CompressionSettings.empty();
                settings.algorithm = value;
              });
            },
          ),
        ));
    widgets.add(algorithmSelector);

    // Algorithm settings
    ListenableBuilder inputs = ListenableBuilder(
        listenable: settings,
        builder: (context, widget) {
          return Column(
            children: settings.getInputWidgets(),
          );
        });
    widgets.add(inputs);

    // Compression start button
    Padding compressButton = Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        children: [
          Expanded(
            child: ElevatedButton.icon(
              onPressed: isCompressing ? null : _compressModels,
              style: ElevatedButton.styleFrom(padding: const EdgeInsets.all(16.0)),
              icon: isCompressing ? Container(
                width: 24,
                height: 24,
                padding: const EdgeInsets.all(2.0),
                child: const CircularProgressIndicator(
                  color: Colors.white,
                  strokeWidth: 3,
                ),
              ) : Container(),
              label: Text(isCompressing ? 'Compressing...' : 'Compress'),
            ),
          )
        ],
      ),
    );
    widgets.add(compressButton);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: widgets,
    );
  }

  Future<void> _compressModels() async {
    // Verify compression settings are correctly set, and display error on throw
    try {
      settings.verify();
    } on ArgumentError catch (err) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(err.message),
          backgroundColor: Colors.orangeAccent,
          duration: const Duration(milliseconds: 3000),
          behavior: SnackBarBehavior.floating,
          width: 300,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      );
      return;
    }

    setState(() {
      isCompressing = true;
    });

    // Compute compression in thread to avoid UI freezing
    int now = DateTime.now().millisecondsSinceEpoch;
    _CompressionPayload payload = _CompressionPayload(now, settings.toJson(), Provider.of<AppModel>(context, listen: false).data);
    List<Model> compressedModels = await compute<_CompressionPayload, List<Model>>((message) {
      CompressionSettings settings = CompressionSettings.fromJson(message.settings);
      return settings.algorithm!
          .compress(message.dataset, settings);
    }, payload);
    setState(() {
      isCompressing = false;
    });

    // Display compression time
    int duration = DateTime.now().millisecondsSinceEpoch - now;
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Compression done in $duration milliseconds.'),
        duration: const Duration(milliseconds: 1500),
        width: 300,
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
    );

    // Store new compression experiment in list
    List<Model> dataset = Provider.of<AppModel>(context, listen: false).data;
    Experiment newExp = Experiment(
        dataset,
        compressedModels,
        name: settings.algorithm!.name,
        duration: duration,
        settings: CompressionSettings.from(
            settings),
        );
    setState(() {
      experiments.add(newExp);
    });
  }
}
