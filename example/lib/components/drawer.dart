import 'package:example/state/model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsDrawer extends Drawer {
  const SettingsDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            width: 400,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Settings', style: TextStyle(color: Colors.white, fontSize: 30),),
            ),
          ),
          CheckboxListTile(
            title: const Text('Display charts'),
            value: Provider.of<AppModel>(context).displayCharts,
            onChanged: (bool? value) {
              Provider.of<AppModel>(context, listen: false).displayCharts = value!;
            },
            secondary: const Icon(Icons.remove_red_eye),
          ),
          CheckboxListTile(
            title: const Text('Display error slope'),
            value: Provider.of<AppModel>(context).errorSlope,
            onChanged: (bool? value) {
              Provider.of<AppModel>(context, listen: false).errorSlope = value!;
            },
            secondary: const Icon(Icons.area_chart),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: ListTile(
                title: const Text("Change dataset"),
                leading: const Icon(Icons.refresh),
                onTap: () {
                  Navigator.of(context).pop();
                  Provider.of<AppModel>(context, listen: false).reset();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}