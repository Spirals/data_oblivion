import 'package:example/chart_view.dart';
import 'package:example/components/drawer.dart';
import 'package:example/state/model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  MaterialApp app = MaterialApp(
    title: "Data oblivion example app",
    home: Scaffold(
      appBar: AppBar(title: Consumer<AppModel>(
        builder: (context, settings, child) {
          return Text(settings.dataset?.label ?? 'Select a dataset');
        },
      )),
      drawer: const SettingsDrawer(),
      body: const SafeArea(
        child: ChartView(),
      ),
    ),
  );

  runApp(ChangeNotifierProvider(create: (context) => AppModel(), child: app,));
}
