# Data oblivion

## Development

This project has been developed using Flutter version `3.10.2`.

```shell
# Install dependencies
flutter pub get

# Run tests
flutter test
```

## Tree construction

To compress models, this algorithm uses a tree memory representation, where models are leaves of the
tree; this models tree is a full binary tree where each node has either no child (thus supporting a
model) or two children, and deepest nodes (count of nodes being `n` or `n-1`) are as far to the left
as possible.

## Further work

* Study the impact of the tree structure on results/performances
* Study the performances of models tree <==> list conversions, and how to limit their number
* Study whether we can check `sizeGain` more often to avoid unneeded compression steps