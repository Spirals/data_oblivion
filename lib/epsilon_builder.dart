import 'dart:math';

/// `EpsilonBuilder` class allows to tune the compression algorithm by modifying
/// the epsilons distribution.
/// It encapsulates a method that associates an epsilon value to any input index.
class EpsilonBuilder {
  final double Function(int index) _method;
  EpsilonBuilder(this._method);

  double get(int index) {
    return _method(index);
  }

  /// Basic linear function.
  factory EpsilonBuilder.linearFromStep(double step) {
    double method (int index) {
      return index * step;
    }
    return EpsilonBuilder(method);
  }

  static EpsilonBuilder get logarithmic {
    return EpsilonBuilder((index) => log(index));
  }

  static EpsilonBuilder get exponential {
    return EpsilonBuilder((index) => exp(index));
  }
}