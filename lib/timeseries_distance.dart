import 'dart:math';

import 'package:data_oblivion/modelling/time_series.dart';
import 'package:flutter/foundation.dart';

class _Payload {
  final TimeSeries ts1;
  final TimeSeries ts2;
  final List<double> timestamps;
  _Payload(this.ts1, this.ts2, this.timestamps);
}

class TimeSeriesDistance extends ChangeNotifier {
  final List<double> _deltas = [];
  final List<num> _squaredDeltas = [];
  bool isDone = false;

  List<double> get errors {
    return _deltas;
  }

  TimeSeriesDistance(TimeSeries ts1, TimeSeries ts2, List<double> timestamps) {
    if (ts1.models.length < ts2.models.length) {
      throw ArgumentError("First timeseries must be bigger than the second.");
    }

    _Payload settings = _Payload(ts1, ts2, timestamps);
    compute<_Payload, List<List<num>>>((message) {
      final List<double> deltas = [];
      final List<num> squaredDeltas = [];

      for (double timestamp in timestamps) {
        double ts1Value = ts1.read(timestamp);
        double ts2Value = ts2.read(timestamp);
        double delta = ((ts1Value - ts2Value)/2).abs();
        deltas.add(delta);
        num squaredDelta = pow((ts1Value - ts2Value), 2);
        squaredDeltas.add(squaredDelta);
      }

      return [deltas, squaredDeltas];
    }, settings).then((value) {
      _deltas.addAll(value[0] as List<double>);
      _squaredDeltas.addAll(value[1]);
      isDone = true;
      notifyListeners();
    });
  }

  double get mean {
    double sum = _deltas.fold(0, (previousValue, element) => previousValue + element);
    return sum/_deltas.length;
  }

  double get mse {
    List<num> deltas = _squaredDeltas.toList();
    deltas.sort();
    double sum = deltas.fold(0, (previousValue, element) => previousValue + element);
    return sum/deltas.length;
  }

  double get rmse {
    return sqrt(mse);
  }

  double get median {
    List<double> deltas = _deltas.toList();
    deltas.sort();
    int index = (deltas.length/2).ceil();
    return index == 0 ? 0 : deltas[index];
  }

  double getPercentile(double value) {
    if (value <= 0 || value > 1) {
      throw ArgumentError("Percentile must be included in the interval [0, 1].");
    }

    List<double> deltas = _deltas.toList();
    deltas.sort();
    int index = (value * deltas.length).ceil();
    return index == 0 ? 0 : deltas[index-1];
  }
}