library data_oblivion;

import 'package:tuple/tuple.dart';

// timestamp => value
typedef Model = Tuple2<double, double>;
