import 'dart:io';

import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';

class PrivamovDataset extends Dataset {
  final bool useLatitude;
  PrivamovDataset({required this.useLatitude}): super(label: "${useLatitude ? 'Latitude' : 'Longitude'} of Privamov's user 1");

  @override
  Future<List<Model>> loadFromFile() async {
    // Use datasets folder from library when called from example app
    String directory = '${Directory.current.path}/${Directory.current.path.contains('example') ? '../' : ''}datasets/';
    List<String> file = File('${directory}privamov-gps-user1').readAsLinesSync();
    List<Model> models = [];

    for (String line in file) {
      List<String> words = line.split(";");
      Model m = Model(double.parse(words[3]), double.parse(words[useLatitude ? 1 : 2]));
      models.add(m);
    }

    return models;
  }
}