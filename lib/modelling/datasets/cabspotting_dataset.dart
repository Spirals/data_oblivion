import 'dart:io';

import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';

class CabspottingDataset extends Dataset {
  final bool useLatitude;
  CabspottingDataset({required this.useLatitude}): super(label: "${useLatitude ? 'Latitude' : 'Longitude'} of Cabspotting's user 0");

  @override
  Future<List<Model>> loadFromFile() async {
    // Use datasets folder from library when called from example app
    String directory = '${Directory.current.path}/${Directory.current.path.contains('example') ? '../' : ''}datasets/';
    List<String> file = File('${directory}cabspotting-user0').readAsLinesSync();
    List<Model> models = [];

    for (String line in file) {
      List<String> words = line.split(";");
      Model m = Model(double.parse(words[3]), double.parse(words[useLatitude ? 1 : 2]));
      models.add(m);
    }

    return models.reversed.toList();
  }
}