import 'dart:io';

import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';

/// E. Keogh, J. Lin and A. Fu (2005).
/// HOT SAX: Efficiently Finding the Most Unusual Time Series Subsequence.
/// In The Fifth IEEE International Conference on Data Mining.
///
/// Dataset was retrieved from https://www.cs.ucr.edu/~eamonn/discords/.
class ECGDataset extends Dataset {
  ECGDataset():
   super(label: 'ECG dataset (HOT SAX paper)');

  @override
  Future<List<Model>> loadFromFile() async {
    // Use datasets folder from library when called from example app
    String directory = '${Directory.current.path}/${Directory.current.path.contains('example') ? '../' : ''}datasets/';
    List<String> file = File('${directory}qtdbsele0606.txt').readAsLinesSync();
    List<Model> models = [];

    for (int i=0; i<file.length; i++) {
      List<String> words = file[i].split('\t');
      Model m = Model(i.toDouble(), double.parse(words[1]));
      models.add(m);
    }

    return models;
  }
}