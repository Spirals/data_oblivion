import 'dart:io';

import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';

class ArtificialDataset extends Dataset {
  final bool useLatitude;
  ArtificialDataset({required this.useLatitude}): super(label: "Test series (${useLatitude ? 'latitude' : 'longitude'})");

  @override
  Future<List<Model>> loadFromFile() async {
    // Use datasets folder from library when called from example app
    String directory = '${Directory.current.path}/${Directory.current.path.contains('example') ? '../' : ''}datasets/';
    List<String> file = File('${directory}artificial_dataset.txt').readAsLinesSync();
    List<Model> models = [];

    for (String line in file) {
      List<String> words = line.split(";");
      Model m = Model(double.parse(words[3]), double.parse(words[useLatitude ? 1 : 2]));
      models.add(m);
    }

    return models;
  }
}