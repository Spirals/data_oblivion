import 'dart:io';

import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';

/// E. Keogh, J. Lin and A. Fu (2005).
/// HOT SAX: Efficiently Finding the Most Unusual Time Series Subsequence.
/// In The Fifth IEEE International Conference on Data Mining.
///
/// Dataset was retrieved from https://www.cs.ucr.edu/~eamonn/discords/.
///
/// The file only contains one value per line, so we create models from them
/// using their location in the file as timestamp.
class SpaceShuttleDataset extends Dataset {
  SpaceShuttleDataset():
   super(label: 'Space shuttle Marotta valve dataset');

  @override
  Future<List<Model>> loadFromFile() async {
    // Use datasets folder from library when called from example app
    String directory = '${Directory.current.path}/${Directory.current.path.contains('example') ? '../' : ''}datasets/';
    List<String> file = File('${directory}TEK16.txt').readAsLinesSync();
    List<Model> models = [];

    for (int i=0; i<file.length; i++) {
      Model m = Model(i.toDouble(), double.parse(file[i]));
      models.add(m);
    }

    return models;
  }
}