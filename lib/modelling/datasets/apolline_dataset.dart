import 'dart:io';

import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/modelling/dataset.dart';
import 'package:flutter/material.dart';

class ApollineDataset extends Dataset {
  ApollineField field;
  ApollineDataset({required this.field}): super(label: '${field.label} in Rennes, April 3rd 2023');

  @override
  Future<List<Model>> loadFromFile() async {
    return readApollineDataset(
        field,
        startDate: DateTime.parse("2023-04-03"),
        endDate: DateTime.parse("2023-04-04")
    );
  }

  @override
  bool get isTimeIndexed {
    return true;
  }
}

List<Model> readApollineDataset(ApollineField field, {int modelsCount = -1, DateTime? startDate, DateTime? endDate}) {
  // Use datasets folder from library when called from example app
  String directory = '${Directory.current.path}/${Directory.current.path.contains('example') ? '../' : ''}datasets/';
  List<String> file = File('${directory}apolline_rennes_2023.csv').readAsLinesSync();
  file.removeAt(0); // remove header
  List<Model> models = [];

  int count = 0;
  for (String line in file) {
    List<String> words = line.split(",");
    DateTime time = DateTime.parse(words[0]);

    if (startDate != null) {
      if (time.millisecondsSinceEpoch < startDate.millisecondsSinceEpoch) {
        continue;
      }
    }
    if (endDate != null) {
      if (time.millisecondsSinceEpoch > endDate.millisecondsSinceEpoch) {
        continue;
      }
    }

    Model m = Model(time.millisecondsSinceEpoch.toDouble(), double.parse(words[field.index]));
    models.add(m);

    count += 1;
    if (count == modelsCount) {
      break;
    }
  }

  return models;
}

// https://stackoverflow.com/a/60528001/11243782
extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}

enum ApollineField {
  time,
  humidity,
  humidityCompensated,
  humidityAdjusted,
  pm01value,
  pm0_3above,
  pm0_5above,
  pm1above,
  pm10above,
  pm10value,
  pm2_5above,
  pm2_5value,
  pm5above,
  pressure,
  temperatureC,
  temperatureK,
  temperatureAdjustedC,
  temperatureDPS310C,
  voltage,
  humidityAM2320,
  temperatureAM2320C,
  latitude,
  longitude
}

extension ApollineFielsUtils on ApollineField {
  String get label {
    switch (this) {
      case ApollineField.time:
      case ApollineField.humidity:
      case ApollineField.pressure:
      case ApollineField.voltage:
      case ApollineField.latitude:
      case ApollineField.longitude:
        return name.capitalize();
      case ApollineField.pm01value:
      case ApollineField.pm10value:
      case ApollineField.pm2_5value:
      case ApollineField.pm5above:
      case ApollineField.pm0_3above:
      case ApollineField.pm0_5above:
      case ApollineField.pm2_5above:
      case ApollineField.pm1above:
      case ApollineField.pm10above:
        bool isAbove = name.endsWith("above");
        String fName = name.substring(0, name.length-5);
        String size = fName.substring(2);
        size = size.replaceAll("_", ".");
        size = size.replaceAll("01", "1");
        return "${isAbove ? "PM above $sizeµm" : "PM$size"} (µg/m³)";
      case ApollineField.temperatureC:
      case ApollineField.temperatureK:
      case ApollineField.temperatureAdjustedC:
      case ApollineField.temperatureAM2320C:
      case ApollineField.temperatureDPS310C:
        String unit = name.characters.last.toUpperCase();
        String fName = name.substring(0, name.length-1);
        if (fName.length == "temperature".length) {
          return "Temperature (°$unit)";
        }
        String prefix = fName.replaceAll("temperature", "");
        return "$prefix temperature (°$unit)";
      case ApollineField.humidityAdjusted:
      case ApollineField.humidityCompensated:
      case ApollineField.humidityAM2320:
        String prefix = name.substring(8);
        return "$prefix humidity";
    }
  }
}