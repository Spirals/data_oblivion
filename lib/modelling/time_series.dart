import 'package:data_oblivion/data_oblivion.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';


/// A time series is a list of data points (t, v) associating a value to a
/// timestamp.
///
/// This class exposes two methods: [read] allows to get the time series value
/// associated to a given timestamp, and [chartData] allows to draw current time
/// series on a chart.
class TimeSeries {
  final List<Model> models;
  final double Function(List<Model> models, double timestamp) _read;
  final List<Tuple2<double, double>> Function(List<Model> models, double firstDataTimestamp, double lastDataTimestamp) _chartView;
  TimeSeries(this.models, this._read, this._chartView);

  /// Piecewise Aggregate Approximation
  /// "[divides a time series] into equal-length segments and recording the mean
  /// value of the datapoints that fall within the segment."
  factory TimeSeries.paa(List<Model> models) {
    return TimeSeries(models, _constantRead, _constantView);
  }

  /// Piecewise Linear Approximation with linear interpolation (all segments are
  /// linked together, as opposed to linear regression)
  factory TimeSeries.pla(List<Model> models) {
    return TimeSeries(models, _linearRead, _linearView);
  }


  /*
  ██████╗ ██╗   ██╗██████╗ ██╗     ██╗ ██████╗    ██╗███╗   ██╗████████╗███████╗██████╗ ███████╗ █████╗  ██████╗███████╗
  ██╔══██╗██║   ██║██╔══██╗██║     ██║██╔════╝    ██║████╗  ██║╚══██╔══╝██╔════╝██╔══██╗██╔════╝██╔══██╗██╔════╝██╔════╝
  ██████╔╝██║   ██║██████╔╝██║     ██║██║         ██║██╔██╗ ██║   ██║   █████╗  ██████╔╝█████╗  ███████║██║     █████╗
  ██╔═══╝ ██║   ██║██╔══██╗██║     ██║██║         ██║██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗██╔══╝  ██╔══██║██║     ██╔══╝
  ██║     ╚██████╔╝██████╔╝███████╗██║╚██████╗    ██║██║ ╚████║   ██║   ███████╗██║  ██║██║     ██║  ██║╚██████╗███████╗
  ╚═╝      ╚═════╝ ╚═════╝ ╚══════╝╚═╝ ╚═════╝    ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝ ╚═════╝╚══════╝
  */

  /// Returns the time series value that is associated to the input [timestamp].
  ///
  /// This function MUST handle timestamps not covered by the current time
  /// series, either because they aren't included in the [t_first, t_last]
  /// interval, or because no time series value matches.
  double read(num timestamp) {
    return _read(models, timestamp.toDouble());
  }

  /// Returns the data points (x, y) that will be displayed on the charts.
  ///
  /// This function MUST return points covering the entire original time
  /// interval (from [firstDataTimestamp] to [lastDataTimestamp], both included).
  List<Tuple2<double, double>> chartData(double firstDataTimestamp, double lastDataTimestamp) {
    return _chartView(models, firstDataTimestamp, lastDataTimestamp);
  }
}


/*
██╗  ██╗███████╗██╗     ██████╗ ███████╗██████╗ ███████╗
██║  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔════╝
███████║█████╗  ██║     ██████╔╝█████╗  ██████╔╝███████╗
██╔══██║██╔══╝  ██║     ██╔═══╝ ██╔══╝  ██╔══██╗╚════██║
██║  ██║███████╗███████╗██║     ███████╗██║  ██║███████║
╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝╚══════╝
 */

// Read helpers
double _constantRead(List<Model> models, double timestamp) {
  // Shortcut for values outside time series range
  if (timestamp < models.first.item1) {
    debugPrint("Using first model to retrieve value of a timestamp that's before the time series.");
    return models.first.item2;
  } else if (timestamp >= models.last.item1) {
    // Not printing anything since it seems reasonable to use last model here
    return models.last.item2;
  }

  // Look for correct model
  for (int i=0; i<models.length-1; i++) {
    Model m1 = models[i];
    Model m2 = models[i+1];
    if (timestamp >= m1.item1 && timestamp < m2.item1) {
      return m1.item2;
    }
  }
  throw StateError("No model matched input timestamp.");
}
double _linearRead(List<Model> models, double timestamp) {
  // Before parsing models, check if value is contained before first model or after last model
  bool valueIsBeforeTS = timestamp < models[0].item1;
  bool valueIsAfterTS = timestamp > models.last.item1;
  if (valueIsBeforeTS || valueIsAfterTS) {
    debugPrint("Need to interpolate a value which timestamp is not included in provided time series range.");
    Model m1 = valueIsBeforeTS ? models.first : models[models.length-2];
    Model m2 = valueIsBeforeTS ? models[1] : models[models.length-1];
    double A = (timestamp - m1.item1) / (m2.item1 - m1.item1);
    double value = m1.item2 + (m2.item2 - m1.item2) * A;
    return value;
  }

  for (int i=0; i<models.length-1; i++) {
    // Check if current point has the searched timestamp
    Model m1 = models[i];
    if (m1.item1 == timestamp) {
      return m1.item2;
    }

    Model m2 = models[i+1];
    // (this check is only useful when reading value of the last point)
    if (m2.item1 == timestamp) {
      return m2.item2;
    }

    // Otherwise, linear interpolate with both models
    if (timestamp > m1.item1 && timestamp < m2.item1) {
      double A = (timestamp - m1.item1) / (m2.item1 - m1.item1);
      double value = m1.item2 + (m2.item2 - m1.item2) * A;
      return value;
    }
  }

  throw StateError("Value was not found.");
}

// Chart formatting helpers
List<Tuple2<double, double>> _constantView(List<Model> models, double firstDataTimestamp, double lastDataTimestamp) {
  List<Tuple2<double, double>> points = [];
  for (int i=0; i<models.length-1; i++) {
    Model m1 = models[i];
    Model m2 = models[i+1];
    points.add(m1);

    if (i == models.length-2) {
      points.add(Tuple2(lastDataTimestamp, m1.item2));
    } else {
      points.add(Tuple2(m2.item1-1, m1.item2));
    }
  }
  return points;
}
List<Tuple2<double, double>> _linearView(List<Model> models, double firstDataTimestamp, double lastDataTimestamp) {
  if (models.first.item1 == firstDataTimestamp && models.last.item1 == lastDataTimestamp) {
    return models;
  }

  List<Model> m = [];

  // Extend first model if needed
  if (models.first.item1 > firstDataTimestamp) {
    debugPrint("Extending first model to give values for timestamps located before the beginning of the time series.");
    m.add(Model(firstDataTimestamp, _linearRead(models, firstDataTimestamp)));
  }

  // Add original data
  m.addAll(models);

  // Extend last model if needed
  if (models.last.item1 < lastDataTimestamp) {
    debugPrint("Extending last model to give values for timestamps located after the end of the time series.");
    m.add(Model(lastDataTimestamp, _linearRead(models, lastDataTimestamp)));
  }

  return m;
}