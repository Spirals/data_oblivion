import 'package:data_oblivion/data_oblivion.dart';
import 'package:flutter/foundation.dart';

/// Represents a file holding time series data.
///
/// This class allows access to time series data through the [loadFromFile]
/// method, which must be implemented by [Dataset] implementations; this method
/// is ran by [data] in a separate thread to avoid UI freezing.
abstract class Dataset {
  /// Name of the dataset, displayed in the example app bar
  final String label;

  /// Time series data
  List<Model> _data = [];

  Dataset({required this.label});

  /// Returns time series data associated to the current dataset.
  /// This method leverages a cache variable to avoid reading data from file
  /// with each call.
  Future<List<Model>> get data async {
    if (_data.isEmpty) {
      _data = await compute<void, List<Model>>((_) {
        return loadFromFile();
      }, null);
    }
    return _data;
  }

  Future<List<Model>> loadFromFile();

  /// Some datasets are not time indexed, so this is the place to say it.
  bool get isTimeIndexed {
    return false;
  }
}
