import '../../data_oblivion.dart';

/// This tries to compress a series of model by merging together models that fit
/// the `epsilon` input error value; it works by iterating over models, moving
/// down the list while models fit the epsilon value, and saving pivot models
/// when they don't.
///
/// Last member of the `models` input list is used to verify if the second to
/// last model can be merged with the others, and thus is not compressed itself.
List<Model> doCompress(double epsilon, List<Model> models) {
  // Input check
  if (models.length < 2) {
    throw ArgumentError("Models list must contain at least 2 models.");
  }

  List<Model> compressed = [];
  Model pivot = models[0];

  for (int i=1; i<=models.length-2; i++) {
    if (!canMerge(epsilon, pivot, models[i], models[i+1])) {
      compressed.add( pivot );
      pivot = models[i];
    }
    // Else, current pivot fits epsilon and can be merged.
  }

  return [...compressed, pivot];
}

/// Returns true if two consecutive models can be merged while respecting input
/// epsilon value, false otherwise.
///
/// A GeoGebra interactive example is available at `/test/resources/figure.ggb`.
bool canMerge(double epsilon, Model m1, Model m2, Model next) {
  // Compute slope of future potential model (ignoring the second one)
  double A = (next.item2 - m1.item2) / (next.item1 - m1.item1);

  // Check if second model fits slope
  double value = (A * (m2.item1 - m1.item1) + m1.item2 - m2.item2).abs();

  return value <= epsilon;
}
