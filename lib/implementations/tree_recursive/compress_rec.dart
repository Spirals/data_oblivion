import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/do_compress.dart';
import 'package:data_oblivion/epsilon_builder.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_list.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_models_tree.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/get_next_model.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';

/// Tries to compress a list of models by removing some of them, using a function
/// creating `epsilon` values, to determine which model can be merged with others.
///
/// This function aims at reaching `sizeGain` objective (in models count) by
/// merging oldest models first, to keep fresh data uncompressed as long as
/// possible.
ModelNode compressRec(EpsilonBuilder builder, int index, int sizeGain, ModelNode tree, {Model? nextModel, int currentGain = 0}) {
  if (!tree.hasChildren) {
    return tree;
  }

  // First try to compress the left subtree
  ModelNode leftCompressedTree = compressRec(builder, index+1, sizeGain, tree.left!, nextModel: getNextModel(tree.right!), currentGain: currentGain);
  currentGain += getModelsListFrom(tree.left!).length - getModelsListFrom(leftCompressedTree).length;
  if (currentGain >= sizeGain) {
    return ModelNode(null, left: leftCompressedTree, right: tree.right!);
  }

  // If it's not enough, compress right subtree as well
  ModelNode rightCompressedTree = compressRec(builder, index+1, sizeGain, tree.right!, nextModel: nextModel, currentGain: currentGain);
  currentGain += getModelsListFrom(tree.right!).length - getModelsListFrom(rightCompressedTree).length;
  if (currentGain >= sizeGain) {
    return ModelNode(null, left: leftCompressedTree, right: rightCompressedTree);
  }

  // Else, further compression is required
  List<Model> models = getModelsListFrom(ModelNode(null, left: leftCompressedTree, right: rightCompressedTree));
  if (nextModel != null) {
    models.add(nextModel);
  }
  List<Model> compressedModels = doCompress(builder.get(index), models);
  return getModelsTreeFrom(compressedModels);
}
