import 'package:data_oblivion/data_oblivion.dart';

class ModelNode {
  final Model? model;
  ModelNode? left;
  ModelNode? right;

  ModelNode(this.model, {this.left, this.right});

  bool get hasChildren {
    return left != null || right != null;
  }

  @override
  String toString() {
    return hasChildren ? '[$left, $right]' : "$model";
  }
}