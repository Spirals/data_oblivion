import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';

/// Returns a tree of models where all input models are tree leaves.
ModelNode getModelsTreeFrom(List<Model> models) {
  if (models.length == 1) {
    return ModelNode(models.first);
  }

  int spliceIndex = (models.length/2).ceil();
  List<Model> leftList = models.sublist(0, spliceIndex);
  List<Model> rightList = models.sublist(spliceIndex);

  return ModelNode(null, left: _buildTree(leftList), right: _buildTree(rightList));
}

/// Recursively builds a tree by splitting models list into two sub-lists, used
/// to build left and right sub-trees.
ModelNode? _buildTree(List<Model> models) {
  if (models.isEmpty) {
    return null;
  }
  if (models.length == 1) {
    return ModelNode(models.first);
  }

  int spliceIndex = (models.length/2).ceil();
  List<Model> leftList = models.sublist(0, spliceIndex);
  List<Model> rightList = models.sublist(spliceIndex);

  return ModelNode(null, left: _buildTree(leftList), right: _buildTree(rightList));
}