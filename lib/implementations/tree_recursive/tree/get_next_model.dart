import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';

/// TODO test!
Model getNextModel(ModelNode tree) {
  if (!tree.hasChildren) {
    return tree.model!;
  }
  return getNextModel(tree.left!);
}