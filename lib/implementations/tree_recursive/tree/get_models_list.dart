import 'package:data_oblivion/data_oblivion.dart';
import 'package:data_oblivion/implementations/tree_recursive/tree/node.dart';

List<Model> getModelsListFrom(ModelNode tree) {
  if (!tree.hasChildren) {
    return [tree.model!];
  }

  List<Model> leftModels = getModelsListFrom(tree.left!);
  List<Model> rightModels = getModelsListFrom(tree.right!);
  return [...leftModels, ...rightModels];
}
