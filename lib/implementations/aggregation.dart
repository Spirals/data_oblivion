import 'package:data_oblivion/data_oblivion.dart';

/// Regroups data by chunks covering `windowTime` duration, their value being an
/// average of underlying modeled values.
///
/// This compression policy is implemented in several time series databases,
/// including as a data retention policy in Timescale:
/// https://docs.timescale.com/use-timescale/latest/data-retention/data-retention-with-continuous-aggregates/
List<Model> aggregationCompress(List<Model> models, int windowTime) {
  if (windowTime <= 0) {
    throw ArgumentError('Time window must be strictly superior to 0.');
  }

  // New list of models
  List<Model> compressed = [];

  // Buffer containing models that respect the [windowTime] constraint and will
  // be merged together when the constraint is broken
  List<Model> buffer = [];

  for (Model m in models) {
    if (buffer.isEmpty) {
      buffer.add(m);
      continue;
    }

    // Check if current model fits time window
    bool matchesBuffer = (m.item1 - buffer.first.item1) < windowTime;

    // If yes, add it to models buffer
    if (matchesBuffer) {
      buffer.add(m);
    }

    // If not, compute average value of the buffer, push it as a compressed
    // model, then clear the buffer
    else {
      double value = buffer.map((e) => e.item2).reduce((value, element) => value + element)/buffer.length;
      compressed.add(Model(buffer.first.item1, value));
      buffer = [m];
    }
  }

  // If there are some elements left in the buffer, compress them together
  if (buffer.isNotEmpty) {
    double value = buffer.map((e) => e.item2).reduce((value, element) => value + element)/buffer.length;
    compressed.add(Model(buffer.first.item1, value));
  }

  return compressed;
}