import 'dart:math';
import 'package:data_oblivion/data_oblivion.dart';


/// This function compresses a time series by removing `modelsCount` models
/// every `sampleSize` models.
List<Model> regularRemovalCompress(List<Model> models, int modelsCount, int sampleSize) {
  if (sampleSize <= modelsCount) {
    throw ArgumentError("The count of models to be removed must be strictly inferior to the sample size.");
  }

  List<Model> compressed = [...models];
  for (int i=compressed.length-sampleSize; i>=0; i-=sampleSize) {
    for (int j=i; j<i+modelsCount && j<compressed.length; j++) {
      compressed.removeAt(j);
    }
  }

  return compressed;
}


/// This compresses a time series by randomly removing values: each model has
/// one chance out of two to be deleted.
List<Model> randomRemovalCompress(List<Model> models) {
  List<Model> compressed = [...models];
  Random random = Random();

  for (int i=compressed.length-1; i>=0; i-=1) {
    if (random.nextBool()) {
      compressed.removeAt(i);
    }
  }

  return compressed;
}


/// Compresses a time series by removing models from the beginning of the series.
List<Model> quotaRemovalCompress(List<Model> models, double quota) {
  if (quota <= 0 || quota >= 1) {
    throw ArgumentError("Quota value must be strictly included between 0 and 1.");
  }
  int index = (models.length * quota).ceil();
  return models.sublist(index);
}