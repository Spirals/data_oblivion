import 'package:data_oblivion/data_oblivion.dart';

/// Compresses data by producing constant models that respect an input error.
///
/// Please note that this algorithm does not build the optimal piecewise
/// polynomial representation of the [models] time series, but rather leverages
/// an [error] threshold.
///
/// Kaushik Chakrabarti, Eamonn Keogh, Sharad Mehrotra, and Michael Pazzani. 2002.
/// Locally adaptive dimensionality reduction for indexing large time series databases.
/// ACM Trans.
/// Database Syst. 27, 2 (June 2002), 188–228.
/// https://doi.org/10.1145/568518.568520
List<Model> acpa(List<Model> models, double error) {
  List<Model> compressed = [];
  Model current = models.first;

  for (Model m in models) {
    if ((m.item2 - current.item2).abs() > error) {
      compressed.add(current);
      current = m;
    }
  }

  compressed.add(current);
  return compressed;
}